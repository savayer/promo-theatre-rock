document.addEventListener('DOMContentLoaded', function() {

    var inputs = document.querySelectorAll('.input_block input, .input_block textarea')

    inputs.forEach(function(input) {

        input.addEventListener('input', function() {
            console.log(this.value)
            this.value ? this.classList.add('has_value') : this.classList.remove('has_value')
        })

    })

})